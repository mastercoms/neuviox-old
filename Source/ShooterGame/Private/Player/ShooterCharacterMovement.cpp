// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Player/ShooterCharacterMovement.h"

#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"

#include "ShooterGame.h"

//----------------------------------------------------------------------//
// UPawnMovementComponent
//----------------------------------------------------------------------//
UShooterCharacterMovement::UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bApplyGravityWhileJumping = true;
	MaxWalkSpeed = 500.0f;
	GravityScale = 1.3f;
	MaxWalkSpeedCrouched = MaxWalkSpeed * 0.5f;
	AirControl = 1.0f;
	AirControlBoostVelocityThreshold = MaxWalkSpeed;
	GroundSpeedCapMultiplier = 1.15f;
	AirSpeedCapMultiplier = 5.0f;
	AirSpeedCap = 60.0f;
	MinStepHeight = 5.0f;
	MaxJumpBoost = 1.7f;
	FallingLateralFriction = 0.0f;
	AirAccelerationMultiplier = 13.0f;
	AirSurfaceMultiplier = 1.25f;
	GroundAccelerationMultiplier = 10.0f;
	GroundSurfaceMultiplier = 1.0f;
	GroundFriction = 4.0f;
	BrakingDecelerationWalking = 800.0f;
	BrakingDecelerationFalling = 0.0f;
	bUseSeparateBrakingFriction = false;
	MaxAcceleration = 2048.0f;
	JumpZVelocity = 440.0f;
	JumpOffJumpZFactor = 1.0f;
	InitialPushForceFactor = 100.0f;
	PushForceFactor = 500.0f;
	RepulsionForce = 0.0f;
	MaxTouchForce = 0.0f;
	TouchForceFactor = 0.0f;
	bMaintainHorizontalGroundVelocity = false;
	bUseFlatBaseForFloorChecks = true;
	NavAgentProps.bCanCrouch = true;
	NavAgentProps.bCanFly = true;
}


float UShooterCharacterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(PawnOwner);
	if (ShooterCharacterOwner)
	{
		if (ShooterCharacterOwner->IsRunning())
		{
			MaxSpeed *= ShooterCharacterOwner->GetRunningSpeedModifier();
		}
	}

	return MaxSpeed;
}

void UShooterCharacterMovement::ApplyComponentBraking(float DeltaTime, float Friction, float BrakingDeceleration, bool ForwardOrStrafe)
{
	FVector ComponentVel = ForwardOrStrafe ? (Velocity | CharacterOwner->GetActorForwardVector()) * CharacterOwner->GetActorForwardVector() :
		(Velocity | CharacterOwner->GetActorRightVector()) * CharacterOwner->GetActorRightVector();
	if (ComponentVel.IsZero() || !HasValidData() || HasAnimRootMotion() || DeltaTime < MIN_TICK_TIME)
	{
		return;
	}

	const float FrictionFactor = FMath::Max(0.0f, BrakingFrictionFactor);
	Friction = FMath::Max(0.0f, Friction * FrictionFactor);
	BrakingDeceleration = FMath::Max(190.5f, ComponentVel.Size());
	const auto bZeroFriction = (Friction == 0.0f);
	const auto bZeroBraking = (BrakingDeceleration == 0.0f);

	if (bZeroFriction && bZeroBraking)
	{
		return;
	}

	const auto OldVel = ComponentVel;

	// subdivide braking to get reasonably consistent results at lower frame
	// rates (important for packet loss situations w/ networking)
	auto RemainingTime = DeltaTime;
	const auto MaxTimeStep = (1.0f / 33.0f);

	// Decelerate to brake to a stop
	const FVector RevAccel = (bZeroBraking ? FVector::ZeroVector : (-FMath::Max(100.0f, BrakingDeceleration) * ComponentVel.GetSafeNormal()));
	while (RemainingTime >= MIN_TICK_TIME)
	{
		// Zero friction uses constant deceleration, so no need for iteration.
		const float dt = ((RemainingTime > MaxTimeStep && !bZeroFriction) ? FMath::Min(MaxTimeStep, RemainingTime * 0.5f) : RemainingTime);
		RemainingTime -= dt;

		// apply friction and braking
		ComponentVel = ComponentVel + ((-Friction) * ComponentVel + RevAccel) * dt;
		Velocity -= (OldVel - ComponentVel);

		// Don't reverse direction
		if ((ComponentVel | OldVel) <= 0.0f)
		{
			ComponentVel = FVector::ZeroVector;
			Velocity -= (OldVel - ComponentVel);
			return;
		}
	}

	// Clamp to zero if nearly zero, or if below min threshold and braking.
	const auto VSizeSq = ComponentVel.SizeSquared();
	if (VSizeSq <= KINDA_SMALL_NUMBER || (!bZeroBraking && VSizeSq <= FMath::Square(BRAKE_TO_STOP_VELOCITY)))
	{
		Velocity -= ComponentVel;
	}
}

void UShooterCharacterMovement::CalcVelocity(float DeltaTime, float Friction, bool bFluid, float BrakingDeceleration)
{
	// Do not update velocity when using root motion or when SimulatedProxy - SimulatedProxy are repped their Velocity
	if (!HasValidData() || HasAnimRootMotion() || DeltaTime < MIN_TICK_TIME || (CharacterOwner && CharacterOwner->Role == ROLE_SimulatedProxy))
	{
		return;
	}

	Friction = FMath::Max(0.f, Friction);
	const float MaxAccel = GetMaxAcceleration();
	float MaxSpeed = GetMaxSpeed();

	// Check if path following requested movement
	bool bZeroRequestedAcceleration = true;
	FVector RequestedAcceleration = FVector::ZeroVector;
	float RequestedSpeed = 0.0f;
	if (ApplyRequestedMove(DeltaTime, MaxAccel, MaxSpeed, Friction, BrakingDeceleration, RequestedAcceleration, RequestedSpeed))
	{
		RequestedAcceleration = RequestedAcceleration.GetClampedToMaxSize(MaxAccel);
		bZeroRequestedAcceleration = false;
	}

	if (bForceMaxAccel)
	{
		// Force acceleration at full speed.
		// In consideration order for direction: Acceleration, then Velocity, then Pawn's rotation.
		if (Acceleration.SizeSquared() > SMALL_NUMBER)
		{
			Acceleration = Acceleration.GetSafeNormal() * MaxAccel;
		}
		else
		{
			Acceleration = MaxAccel * (Velocity.SizeSquared() < SMALL_NUMBER ? UpdatedComponent->GetForwardVector() : Velocity.GetSafeNormal());
		}

		AnalogInputModifier = 1.f;
	}

	// Path following above didn't care about the analog modifier, but we do for everything else below, so get the fully modified value.
	// Use max of requested speed and max speed if we modified the speed in ApplyRequestedMove above.
	MaxSpeed = FMath::Max3(RequestedSpeed, MaxSpeed * AnalogInputModifier, GetMinAnalogSpeed());

	// Apply braking or deceleration
	const bool bZeroAcceleration = Acceleration.IsZero();
	const FVector ForwardAccel = (Acceleration | CharacterOwner->GetActorForwardVector()) * CharacterOwner->GetActorForwardVector();
	const FVector StrafeAccel = Acceleration - ForwardAccel;
	const bool bZeroForwardAccel = ForwardAccel.IsZero();
	const bool bZeroStrafeAccel = StrafeAccel.IsZero();
	const bool bVelocityOverMax = IsExceedingMaxSpeed(MaxSpeed);

	// Only apply braking if there is no acceleration, or we are over our max speed and need to slow down to it.
	if (IsMovingOnGround() && bBrakingFrameTolerated)
	{
		if ((bZeroAcceleration && bZeroRequestedAcceleration) || bVelocityOverMax)
		{
			const FVector OldVelocity = Velocity;

			const float ActualBrakingFriction = (bUseSeparateBrakingFriction ? BrakingFriction : Friction);
			ApplyVelocityBraking(DeltaTime, ActualBrakingFriction, BrakingDeceleration);

			// Don't allow braking to lower us below max speed if we started above it.
			if (bVelocityOverMax && Velocity.SizeSquared() < FMath::Square(MaxSpeed) && FVector::DotProduct(Acceleration, OldVelocity) > 0.0f)
			{
				Velocity = OldVelocity.GetSafeNormal() * MaxSpeed;
			}
		}
		else if (!bZeroAcceleration && bZeroForwardAccel)
		{
			const FVector OldVelocity = Velocity;

			const float ActualBrakingFriction = (bUseSeparateBrakingFriction ? BrakingFriction : Friction);
			ApplyComponentBraking(DeltaTime, ActualBrakingFriction, BrakingDeceleration, true);

			// Don't allow braking to lower us below max speed if we started above it.
			if (bVelocityOverMax && Velocity.SizeSquared() < FMath::Square(MaxSpeed) && FVector::DotProduct(Acceleration, OldVelocity) > 0.0f)
			{
				Velocity = OldVelocity.GetSafeNormal() * MaxSpeed;
			}
		}
		else if (!bZeroAcceleration && bZeroStrafeAccel)
		{
			const FVector OldVelocity = Velocity;

			const float ActualBrakingFriction = (bUseSeparateBrakingFriction ? BrakingFriction : Friction);
			ApplyComponentBraking(DeltaTime, ActualBrakingFriction, BrakingDeceleration, false);

			// Don't allow braking to lower us below max speed if we started above it.
			if (bVelocityOverMax && Velocity.SizeSquared() < FMath::Square(MaxSpeed) && FVector::DotProduct(Acceleration, OldVelocity) > 0.0f)
			{
				Velocity = OldVelocity.GetSafeNormal() * MaxSpeed;
			}
		}
		else if (!bZeroAcceleration)
		{
			// Friction affects our ability to change direction. This is only done for input acceleration, not path following.
			const FVector AccelDir = Acceleration.GetSafeNormal();
			const float VelSize = Velocity.Size();
			Velocity = Velocity - (Velocity - AccelDir * VelSize) * FMath::Min(DeltaTime * Friction, 1.f);
		}
	}

	// Apply fluid friction
	if (bFluid)
	{
		Velocity = Velocity * (1.f - FMath::Min(Friction * DeltaTime, 1.f));
	}

	// How fast should be going
	float MaxSpeedTolerated = MaxSpeed;
	if (IsMovingOnGround() && bBrakingFrameTolerated)
	{
		MaxSpeedTolerated *= GroundSpeedCapMultiplier;
	}
	else
	{
		MaxSpeedTolerated *= AirSpeedCapMultiplier;
	}
	// Apply acceleration
	if (IsFalling() || !bBrakingFrameTolerated || bUseAdvancedAccelerationOnGround)
	{
		// Clamp acceleration to max speed
		Acceleration = Acceleration.GetClampedToMaxSize2D(MaxSpeedTolerated);
		// Find veer
		const FVector AccelDir = Acceleration.GetSafeNormal2D();
		const float Veer = Velocity.X * AccelDir.X + Velocity.Y * AccelDir.Y;
		// Get add speed with air speed cap
		const float AddSpeed = (IsMovingOnGround() && bBrakingFrameTolerated ? Acceleration : Acceleration.GetClampedToMaxSize2D(AirSpeedCap)).Size2D() - Veer;
		if (AddSpeed > 0.0f)
		{
			float SurfaceFriction = IsMovingOnGround() && bBrakingFrameTolerated ? GroundSurfaceMultiplier : AirSurfaceMultiplier;
			// Apply acceleration
			Acceleration *= (IsMovingOnGround() && bBrakingFrameTolerated ? GroundAccelerationMultiplier : AirAccelerationMultiplier) * DeltaTime * SurfaceFriction;
			Acceleration = Acceleration.GetClampedToMaxSize2D(AddSpeed);
			Velocity += Acceleration;
		}
	}
	else
	{
		Velocity += Acceleration * DeltaTime * GroundAccelerationMultiplier;
	}
	Velocity += RequestedAcceleration * DeltaTime;
	// Don't let us speed up too much
	if (IsFalling())
	{
		Velocity = Velocity.GetClampedToMaxSize(Velocity.Size() * MaxJumpBoost);
	}
	// We do have a upper speed limit, no infinite speed
	if (!bZeroAcceleration || !bZeroRequestedAcceleration)
	{
		Velocity = Velocity.GetClampedToMaxSize(MaxSpeedTolerated);
	}

	if (bUseRVOAvoidance)
	{
		CalcAvoidanceVelocity(DeltaTime);
	}

	// Delay ground considerations by a frame to allow for bunnyhopping
	bBrakingFrameTolerated = IsMovingOnGround();

	float Speed = Velocity.Size2D();

	// Dynamic step height code for allowing sliding away from a slope when at a high speed
	if (Speed <= MaxWalkSpeedCrouched)
	{
		// If we're crouching, just use max
		MaxStepHeight = GetClass()->GetDefaultObject<UShooterCharacterMovement>()->MaxStepHeight;
	}
	else
	{
		// Scale step height down the faster we go
		float SpeedMultiplier = 0.75f;
		const AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(PawnOwner);
		if (ShooterCharacterOwner)
		{
			SpeedMultiplier = (Speed / (MaxWalkSpeed * ShooterCharacterOwner->GetRunningSpeedModifier()));
		}
		MaxStepHeight = FMath::Clamp(GetClass()->GetDefaultObject<UShooterCharacterMovement>()->MaxStepHeight / SpeedMultiplier, MinStepHeight, GetClass()->GetDefaultObject<UShooterCharacterMovement>()->MaxStepHeight);
	}

	// Code for maintaining Z velocity when moving off a ramp
	float TrimpZ = GetCharacterOwner()->GetCapsuleComponent()->GetPhysicsLinearVelocity().Z;
	// We don't have any Z vel
	if (TrimpZ > 0.0f)
	{
		FHitResult HitResult;
		float AdditionalZToRemove = 0.0f;
		// We don't have a default floor, so find if we are close enough to one to allow someone to jump slide off a ramp
		if (IsFalling() && GetCharacterOwner()->JumpCurrentCount > 0)
		{
			HitResult = FHitResult(ForceInit);
			FVector Start = GetCharacterOwner()->GetCapsuleComponent()->GetComponentLocation() - FVector(0.0f, 0.0f, GetCharacterOwner()->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());
			float FloorSweepTraceDist = MaxStepHeight + MAX_FLOOR_DIST + KINDA_SMALL_NUMBER;
			FVector End = Start - FVector(0.0f, 0.0f, FloorSweepTraceDist);
			GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_WorldStatic, FCollisionQueryParams(FName(TEXT("TrimpTrace")), true, GetCharacterOwner()));
			if (!HitResult.bBlockingHit)
			{
				return;
			}
			// If we are in the +Z part of a jump, then remove that part of the Z vel from the trimp
			if (TrimpZ - JumpZVelocity > 0)
			{
				AdditionalZToRemove = JumpZVelocity;
			}
		}
		else
		{
			// We're on the ground, use the default floor
			HitResult = CurrentFloor.HitResult;
		}

		// Compare slopes of where we were vs where we are now
		float FloorDot = HitResult.ImpactNormal | LastRampNormal;
		LastRampNormal = HitResult.ImpactNormal;
		// Don't get too crazy (90 degree angle trimps) and ignore flat movement
		if (FloorDot > 0.0f && FloorDot < 1.0f)
		{
			// Remove extra jump vel from Trimp, then scale it based on the slope. Since this is an impulse, it does not scale with delta time
			// Then add a frame of our horizontal velocity as vertical velocity based on the slope
			// Then add a frame of gravity downwards vel
			TrimpZ = (TrimpZ - AdditionalZToRemove) * (1.0f - FloorDot) + Speed * DeltaTime * (1.0f - FloorDot) + GetGravityZ() * DeltaTime;
			if (TrimpZ > 0.0f)
			{
				AddImpulse(FVector(0.0f, 0.0f, TrimpZ), true);
			}
		}

	}
	
}

void UShooterCharacterMovement::OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode)
{
	Super::OnMovementModeChanged(PreviousMovementMode, PreviousCustomMode);
	// If we started falling, make sure we continue moving up if applicable
	if (PreviousCustomMode == EMovementMode::MOVE_Walking && MovementMode == EMovementMode::MOVE_Falling)
	{
		
	}
}
