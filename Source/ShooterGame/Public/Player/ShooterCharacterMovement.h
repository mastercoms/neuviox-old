// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

/**
 * Movement component meant for use with Pawns.
 */

#pragma once

#include "GameFramework/CharacterMovementComponent.h"

#include "ShooterCharacterMovement.generated.h"

UCLASS()
class UShooterCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

	virtual float GetMaxSpeed() const override;
	virtual void CalcVelocity(float DeltaTime, float Friction, bool bFluid, float BrakingDeceleration) override;
	virtual void ApplyComponentBraking(float DeltaTime, float Friction, float BrakingDeceleration, bool ForwardOrStrafe);
	virtual void OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode) override;

	/** Skip braking for one tick on ground */
	bool bBrakingFrameTolerated;

	/** last impact normal from floor */
	FVector LastRampNormal;

	/** the multiplier for acceleration while on ground */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	float GroundAccelerationMultiplier;

	/** acceleration multiplier due to ground surface friction */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	float GroundSurfaceMultiplier;

	/** the minimum step height from moving fast */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	float MinStepHeight;

	/** multiplier for upper speed cap while on ground */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	float GroundSpeedCapMultiplier;

	/** if arena movement should be used on the ground */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	bool bUseAdvancedAccelerationOnGround;

	/** multiplier for upper speed cap while in air */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere, BlueprintReadWrite)
	float AirSpeedCapMultiplier;

	/** maximum acceleration vector magnitude in air */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere, BlueprintReadWrite)
	float AirSpeedCap;

	/** the multiplier for acceleration in air */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere, BlueprintReadWrite)
	float AirAccelerationMultiplier;

	/** acceleration multiplier due to air surface friction */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere, BlueprintReadWrite)
	float AirSurfaceMultiplier;

	/** maximum velocity (in percent) gained from jumping in a single tick */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere, BlueprintReadWrite)
	float MaxJumpBoost;
};

