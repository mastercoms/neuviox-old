// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class ShooterGameEditorTarget : TargetRules
{
	public ShooterGameEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("ShooterGame");
		
		bAdaptiveUnityDisablesPCH = false;
		bAdaptiveUnityCreatesDedicatedPCH = true;
		//bUseBackwardsCompatibleDefaults = false;
		bIWYU = true;
		
		if (Configuration == UnrealTargetConfiguration.Shipping || Configuration == UnrealTargetConfiguration.Test)
		{
			bAllowLTCG = true;
		}
		if (Configuration != UnrealTargetConfiguration.DebugGame || Configuration != UnrealTargetConfiguration.Debug)
		{
			bUseFastPDBLinking = true;
		}
	}
}
